from django.urls import path
from booking.views import Rides

urlpatterns = [
    path('rides/', Rides.as_view(), name="Rides"),
]
