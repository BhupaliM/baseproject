import boto3

AWS_ACCESS_KEY_ID="anything"
AWS_SECRET_ACCESS_KEY="anything"
REGION_NAME="ap-south-1"
ENDPOINT_URL="http://localhost:8042"

TABLE_NAME = 'BaseRides'
LSI_PassengerRidesStatus = 'PassengerRidesStatusIndex'
GSI_DriverRides = 'DriverRidesIndex'
GSI_DriverRidesStatus = 'DriverRideStatusIndex'


dynamodb_client = boto3.client('dynamodb',
                          aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                          region_name=REGION_NAME,
                          endpoint_url=ENDPOINT_URL)
dynamodb_resource = boto3.resource('dynamodb',
                          aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                          region_name=REGION_NAME,
                          endpoint_url=ENDPOINT_URL)
