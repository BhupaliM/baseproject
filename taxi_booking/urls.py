"""taxi_booking URL Configuration"""

from django.contrib import admin
from django.urls import path, include
from booking.urls import urlpatterns as booking_urlpatterns
urlpatterns = [
    path('admin/', admin.site.urls),
    path('ride-api/', include(booking_urlpatterns)),
]
