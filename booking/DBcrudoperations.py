from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError
import uuid
from booking.DBConnections import TABLE_NAME,LSI_PassengerRidesStatus,GSI_DriverRides,GSI_DriverRidesStatus,dynamodb_client,dynamodb_resource

def generate_uuid():
    return str(uuid.uuid4())


def insert_new_ride(**kwargs):
    # while entering data item it always overrides existing primary values and does not throw error of existing values
    table = dynamodb_resource.Table(TABLE_NAME)
    RideId = generate_uuid()
    kwargs['RideId'] = RideId

    try:
        response = table.put_item(
        Item= dict(kwargs)
    )

    except Exception as e:
        return "Sorry ! Could not create entry"
    return "Entry created successfully with RideId {}".format(kwargs['RideId'])


def get_ride_details(**kwargs):
    table = dynamodb_resource.Table(TABLE_NAME)
    passid = kwargs.get('PassengerId')
    driverid = kwargs.get('DriverId')
    rideid = kwargs.get('RideId')
    ridestatus = kwargs.get('RideStatus')

    # CONDITION 1 only on basis of PassengerID ie get all rides for a given PassengerId
    if passid and not rideid and not driverid and not ridestatus:
        try:
            response = table.query(
                KeyConditionExpression=Key('PassengerId').eq(passid)
            )
        except ClientError as e:
            print(e.response['Error']['Message'])

    # CONDITION 2 only on basis of DriverId ie get all rides for a given DriverId
    elif driverid and not rideid and not passid and not ridestatus:
        try:
            response = table.query(
                IndexName=GSI_DriverRides,
                KeyConditionExpression=Key('DriverId').eq(driverid)
            )

        except ClientError as e:
            print(e.response['Error']['Message'])

    # CONDITION 3  on basis of PassengerId and RideId both ie get the ride for a given combination of PassengerId and RideId
    elif passid and rideid and not driverid and not ridestatus:
        try:
            response = table.query(
                KeyConditionExpression=Key('PassengerId').eq(passid) & Key('RideId').eq(rideid)
            )
        except ClientError as e:
            print(e.response['Error']['Message'])

    # CONDITION 4  on basis of DriverId and RideId both ie get the ride for a given combination of DriverId and RideId
    elif driverid and rideid and not passid and not ridestatus:
        try:
            response = table.query(
                IndexName=GSI_DriverRides,
                KeyConditionExpression=Key('DriverId').eq(driverid) & Key('RideId').eq(rideid)
            )

        except ClientError as e:
            print(e)
            print(e.response['Error']['Message'])

    # CONDITION 5  on basis of PassengerId and RideStatus both ie get the rides for a given combination of PassengerId and RideStatus
    elif passid and ridestatus and not driverid and not rideid:
        try:
            response = table.query(
                IndexName=LSI_PassengerRidesStatus,
                KeyConditionExpression=Key('PassengerId').eq(passid) & Key('RideStatus').eq(ridestatus)
            )

        except Exception as e:
            print(e)
            print(e.response['Error']['Message'])

    # CONDITION 6  on basis of DriverId and RideStatus both ie get the rides for a given combination of DriverId and RideStatus
    elif driverid and ridestatus and not passid and not rideid:
        try:
            response = table.query(
                IndexName=GSI_DriverRidesStatus,
                KeyConditionExpression=Key('DriverId').eq(driverid) & Key('RideStatus').eq(ridestatus)
            )

        except Exception as e:
            print(e)
            print(e.response['Error']['Message'])

    return response['Items']

