from booking.DBConnections import TABLE_NAME,LSI_PassengerRidesStatus,GSI_DriverRides,GSI_DriverRidesStatus,dynamodb_client,dynamodb_resource
import time

def create_table_and_indexes():
    # skip if table already exists
    try:
        response = dynamodb_client.describe_table(TableName=TABLE_NAME)
        # table exists...bail
        print("Table [{}] already exists. Skipping table creation.".format(TABLE_NAME))
        return
    except:
        pass  # no table... good
    print("Creating table [{}]".format(TABLE_NAME))
    response = dynamodb_client.create_table(
        TableName=TABLE_NAME,
        KeySchema=[
            {
                'KeyType': 'HASH',
                'AttributeName': 'PassengerId'
            },
            {
                'KeyType': 'RANGE',
                'AttributeName': 'RideId'
            }
        ],
        LocalSecondaryIndexes=[
            {
                'IndexName': LSI_PassengerRidesStatus,
                'KeySchema': [
                    {
                        'KeyType': 'HASH',
                        'AttributeName': 'PassengerId'
                    },
                    {
                        'KeyType': 'RANGE',
                        'AttributeName': 'RideStatus'
                    }
                ],
                # Note: since we are projecting some attributes from table and not all
                'Projection': {
                    'ProjectionType': 'ALL',
                    # 'NonKeyAttributes': ['DriverId','CancelledBy']
                }
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'PassengerId',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'RideStatus',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'RideId',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 2,
            'WriteCapacityUnits': 2
        }
    )
    print("Waiting for table [{}] to be created".format(TABLE_NAME))
    waiter = dynamodb_client.get_waiter('table_exists')
    waiter.wait(TableName=TABLE_NAME)
    # if no exception, continue
    print("Table created, now creating global indexes")
    create_gsi1()
    print("Now creating second global secondary index")
    time.sleep(60)
    create_gsi2()

def create_gsi1():
    try:
        resp = dynamodb_client.update_table(
            TableName=TABLE_NAME,
            # Any attributes used in your new global secondary index must be declared in AttributeDefinitions
            AttributeDefinitions=[
                {'AttributeName': 'DriverId', 'AttributeType': 'S'},
                {'AttributeName': 'RideId', 'AttributeType': 'S'},
                {'AttributeName': 'RideStatus', 'AttributeType': 'S'},
                {'AttributeName': 'PassengerId', 'AttributeType': 'S'}
            ],
            # This is where you add, update, or delete any global secondary indexes on your table.
            GlobalSecondaryIndexUpdates=[
                {
                    "Create": {
                        # You need to name your index and specifically refer to it when using it for queries.
                        "IndexName": GSI_DriverRides,
                        # Like the table itself, you need to specify the key schema for an index.
                        # For a global secondary index, you can use a simple or composite key schema.
                        'KeySchema': [{
                            'AttributeName': 'DriverId',
                            'KeyType': 'HASH'
                        },
                            {
                                'AttributeName': 'RideId',
                                'KeyType': 'RANGE'
                            }
                        ],
                        # You can choose to copy only specific attributes from the original item into the index.
                        # You might want to copy only a few attributes to save space.
                        "Projection": {
                            "ProjectionType": "ALL"
                        },
                        # Global secondary indexes have read and write capacity separate from the underlying table.
                        "ProvisionedThroughput": {
                            "ReadCapacityUnits": 1,
                            "WriteCapacityUnits": 1,
                        }
                    }
                },

            ],
        )
        print(f"Secondary index {GSI_DriverRides} added!")
    except Exception as e:
        print("Error updating table:")
        print(e)

def create_gsi2():
    try:
        resp = dynamodb_client.update_table(
            TableName=TABLE_NAME,
            # Any attributes used in your new global secondary index must be declared in AttributeDefinitions
            AttributeDefinitions=[
                {'AttributeName': 'DriverId', 'AttributeType': 'S'},
                {'AttributeName': 'RideId', 'AttributeType': 'S'},
                {'AttributeName': 'RideStatus', 'AttributeType': 'S'},
                {'AttributeName': 'PassengerId', 'AttributeType': 'S'}
            ],
            # This is where you add, update, or delete any global secondary indexes on your table.
            GlobalSecondaryIndexUpdates=[

                {
                    "Create": {

                        # You need to name your index and specifically refer to it when using it for queries.
                        "IndexName": GSI_DriverRidesStatus,
                        # Like the table itself, you need to specify the key schema for an index.
                        # For a global secondary index, you can use a simple or composite key schema.
                        'KeySchema': [{
                            'AttributeName': 'DriverId',
                            'KeyType': 'HASH'
                        },
                            {
                                'AttributeName': 'RideStatus',
                                'KeyType': 'RANGE'
                            }
                        ],
                        # You can choose to copy only specific attributes from the original item into the index.
                        # You might want to copy only a few attributes to save space.
                        "Projection": {
                            "ProjectionType": "ALL"
                        },
                        # Global secondary indexes have read and write capacity separate from the underlying table.
                        "ProvisionedThroughput": {
                            "ReadCapacityUnits": 1,
                            "WriteCapacityUnits": 1,
                        }
                    }
                },
            ],
        )
        print(f"Secondary index {GSI_DriverRidesStatus} added!")
    except Exception as e:
        print("Error updating table:")
        print(e)

create_table_and_indexes()