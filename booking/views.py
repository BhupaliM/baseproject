from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
import json
from booking.DBcrudoperations import insert_new_ride,get_ride_details


class Rides(APIView):

    def post(self, request):
        data = json.loads(request.body)
        resp = insert_new_ride(**data)
        return Response(resp)

    def get(self, request):
        data = {
                    "PassengerId":request.query_params.get("PassengerId"),
                    "DriverId":request.query_params.get("DriverId"),
                    "RideId":request.query_params.get("RideId"),
                    "RideStatus":request.query_params.get("RideStatus")
                }

        resp = get_ride_details(**data)
        return Response(resp)
